package com.cumdy.rcms.fragment.days;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cumdy.rcms.R;
import com.cumdy.rcms.adapter.TimetableAdapter;
import com.cumdy.rcms.adapter.TimetableHolder;
import com.cumdy.rcms.model.Period;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by banyar on 5/2/17.
 */

public class WednesdayFragment extends Fragment {
    private TimetableAdapter mAdapter;
    private RecyclerView rv;
    private DatabaseReference ref;
    private String teacherID;
    private Bundle bundle;

    public WednesdayFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        teacherID = bundle.getString("teacherID");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_days, container, false);

        rv = (RecyclerView) v.findViewById(R.id.rc_days);
        rv.setHasFixedSize(true);
        ref = FirebaseDatabase.getInstance().getReference().child("timetable").child(teacherID).child("Wednesday");
        mAdapter = new TimetableAdapter(Period.class, R.layout.rc_item_period, TimetableHolder.class, ref, getContext());
        RecyclerView.LayoutManager mLM = new LinearLayoutManager(getContext());
        rv.setLayoutManager(mLM);
        rv.setAdapter(mAdapter);

        return v;
    }
}
