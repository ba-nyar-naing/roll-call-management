package com.cumdy.rcms.fragment.admin.teacher.adapter;

import android.content.Context;

import com.cumdy.rcms.model.Teacher;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by banyar on 6/4/17.
 */

public class AdminAllTeacherAdapter extends FirebaseRecyclerAdapter<Teacher, AdminAllTeacherHolder> {

    private Context context;

    public AdminAllTeacherAdapter(Class<Teacher> modelClass, int modelLayout, Class<AdminAllTeacherHolder> viewHolder, DatabaseReference ref, Context context) {
        super(modelClass, modelLayout, viewHolder, ref);
        this.context = context;
    }

    @Override
    protected void populateViewHolder(AdminAllTeacherHolder holder, Teacher t, int position) {
        holder.txtTeacherID.setText(t.getId());
        holder.txtTeacherName.setText(t.getName());
        holder.txtTeacherDept.setText(t.getDept());
        holder.txtTeacherPost.setText(t.getPosition());
        holder.txtTeacherMail.setText(t.getEmail());
        holder.txtTeacherPassword.setText(t.getPassword());
    }
}