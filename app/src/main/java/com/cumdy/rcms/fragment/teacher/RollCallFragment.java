package com.cumdy.rcms.fragment.teacher;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.cumdy.rcms.activity.RollCallerActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by banyar on 4/29/17.
 */

public class RollCallFragment extends Fragment {

    private String day, month, period, year, section;
    private Button btnDate, btnPeriod, btnYear, btnSection, btnStartCall;
    private List<String> sectionList;
    private DatabaseReference ref;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_roll_call, container, false);

        progressDialog = new ProgressDialog(v.getContext());
        progressDialog.setCancelable(false);

        btnDate = (Button) v.findViewById(R.id.frc_date);
        btnPeriod = (Button) v.findViewById(R.id.frc_period);
        btnYear = (Button) v.findViewById(R.id.frc_year);
        btnSection = (Button) v.findViewById(R.id.frc_section);
        btnStartCall = (Button) v.findViewById(R.id.frc_start_call);

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                int index = calendar.get(Calendar.MONTH);
                month = getMonth(index);

                btnDate.setText(day + " / " + month);
            }
        });

        btnPeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items("1", "2", "1-2", "3", "4", "3-4", "5", "6", "5-6", "6", "6-7", "5-6-7")
                        .title("Select Period")
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                btnPeriod.setText(text.toString());
                            }
                        })
                        .show();
            }
        });

        btnYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items("1CST", "2CS", "2CT", "3CS", "3CT", "4CS", "4CT", "5CS", "5CT")
                        .title("Select Year")
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                btnYear.setText(text.toString());
                                btnSection.setText("Section");
                                year = text.toString();
                                getConfigSectionList();
                            }
                        })
                        .show();
            }
        });

        btnSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnYear.getText().toString().equals("Year")) {
                    Toast.makeText(v.getContext(), "Please choose YEAR first", Toast.LENGTH_SHORT).show();
                } else {
                    new MaterialDialog.Builder(getContext())
                            .items(sectionList)
                            .title("Select Section")
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    btnSection.setText(text.toString());
                                }
                            })
                            .show();
                }
            }
        });

        btnStartCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                String text = " ";
                period = btnPeriod.getText().toString();
                year = btnYear.getText().toString();
                section = btnSection.getText().toString();

                if (btnDate.getText().toString().equals("Date")) {
                    Toast.makeText(v.getContext(), "Please choose date", Toast.LENGTH_LONG).show();
                } else if (period.equals("Period")) {
                    Toast.makeText(v.getContext(), "Please choose period", Toast.LENGTH_LONG).show();
                } else if (year.equals("Year")) {
                    Toast.makeText(v.getContext(), "Please choose year", Toast.LENGTH_LONG).show();
                } else if (section.equals("Section")) {
                    Toast.makeText(v.getContext(), "Please choose section", Toast.LENGTH_LONG).show();
                } else {
                    text = "Roll call for Period " + period + "\nClass " + year + " - " + section + "\nDate " + day + " / " + month;
                    new MaterialDialog.Builder(getContext())
                            .content(text)
                            .title("Check again before start")
                            .negativeText("Cancel")
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Context context = v.getContext();
                                    Intent i = new Intent(context, RollCallerActivity.class);
                                    i.putExtra("day", day);
                                    i.putExtra("month", month);
                                    i.putExtra("period", period);
                                    i.putExtra("year", year);
                                    i.putExtra("section", section);
                                    context.startActivity(i);
                                }
                            })
                            .show();
                }
            }
        });

        return v;
    }


    private void getConfigSectionList() {
        progressDialog.setMessage("Initializing config data...\nInternet connection is required.");
        progressDialog.show();
        sectionList = new ArrayList<>();

        ref = FirebaseDatabase.getInstance().getReference();
        ref = ref.child("config/class").child(year);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sectionList = (ArrayList<String>) dataSnapshot.getValue();
                progressDialog.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private String getMonth(int index) {
        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return months[index];
    }
}
