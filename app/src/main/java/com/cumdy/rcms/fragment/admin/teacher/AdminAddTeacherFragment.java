package com.cumdy.rcms.fragment.admin.teacher;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cumdy.rcms.R;
import com.cumdy.rcms.model.Teacher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by banyar on 5/6/17.
 */

public class AdminAddTeacherFragment extends Fragment {

    private EditText edTrID, edTrName, edTrDept, edTrPost, edTrEmail, edTrPassword;
    private Button btnSubmit;
    private String id, name, dept, post, email, password, uid;
    private ProgressDialog progressDialog;
    private DatabaseReference ref;
    private FirebaseAuth auth;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_add_teacher, container, false);

        progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setMessage("Uploading data to server...\nInternet connection is required");
        progressDialog.setCancelable(false);

        edTrID = (EditText) view.findViewById(R.id.ed_fg_admin_add_teacher_id);
        edTrName = (EditText) view.findViewById(R.id.ed_fg_admin_add_teacher_name);
        edTrDept = (EditText) view.findViewById(R.id.ed_fg_admin_add_teacher_department);
        edTrPost = (EditText) view.findViewById(R.id.ed_fg_admin_add_teacher_position);
        edTrEmail = (EditText) view.findViewById(R.id.ed_fg_admin_add_teacher_email);
        edTrPassword = (EditText) view.findViewById(R.id.ed_fg_admin_add_teacher_password);
        btnSubmit = (Button) view.findViewById(R.id.btn_fg_admin_add_teacher_submit);

        auth = FirebaseAuth.getInstance();


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                id = edTrID.getText().toString();
                name = edTrName.getText().toString();
                dept = edTrDept.getText().toString();
                post = edTrPost.getText().toString();
                email = edTrEmail.getText().toString();
                password = edTrPassword.getText().toString();

                if (validate()) {
                    progressDialog.show();
                    final Teacher data = new Teacher(id, name, dept, post, email, password);

                    auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                uid = auth.getCurrentUser().getUid();
                                data.setUid(uid);
                                upload(data);
                                auth.signInWithEmailAndPassword("admin@rcms.com", "admin1");
                                progressDialog.cancel();
                                Toast.makeText(v.getContext(), "Account created successfully!", Toast.LENGTH_SHORT).show();
                            } else {
                                progressDialog.cancel();
                                Toast.makeText(v.getContext(), "Account creation fail!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        return view;
    }

    private boolean validate() {
        // TODO: 5/7/17 validate data from editText

        return true;
    }

    private void upload(Teacher data) {
        ref = FirebaseDatabase.getInstance().getReference();
        ref.child("teacher").child(id).setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.cancel();
            }
        });
    }


}
