package com.cumdy.rcms.fragment.admin;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.cumdy.rcms.model.Period;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by banyar on 5/6/17.
 */

public class AdminAddTimeTableFragment extends Fragment {

    private EditText edTtId, edTtSubName;
    private Button btnDay, btnPeriod, btnYear, btnSection, btnSubmit;
    private String id, subject_name, day, period, year, section, time, class_name;
    private DatabaseReference ref;
    private ProgressDialog progressDialog;
    private ArrayList<String> sectionList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_add_timetable, container, false);

        progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setCancelable(false);

        edTtId = (EditText) view.findViewById(R.id.ed_fg_admin_add_timetable_teacher_id);
        edTtSubName = (EditText) view.findViewById(R.id.ed_fg_admin_add_timetable_subject_name);
        btnDay = (Button) view.findViewById(R.id.btn_fg_admin_add_timetable_day);
        btnPeriod = (Button) view.findViewById(R.id.btn_fg_admin_add_timetable_period);
        btnSubmit = (Button) view.findViewById(R.id.btn_fg_admin_add_timetable_submit);
        btnYear = (Button) view.findViewById(R.id.btn_fg_admin_add_timetable_year);
        btnSection = (Button) view.findViewById(R.id.btn_fg_admin_add_timetable_section);

        btnDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items("Monday", "Tuesday", "Wednesday", "Thursday", "Friday")
                        .title("Select Day")
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                btnDay.setText(text.toString());
                            }
                        })
                        .show();
            }
        });

        btnPeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items("1", "2", "1-2", "3", "4", "3-4", "5", "6", "5-6", "6", "6-7", "5-6-7")
                        .title("Select Period")
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                btnPeriod.setText(text.toString());
                                period = text.toString();
                                getConfigTime();
                            }
                        })
                        .show();
            }
        });

        btnYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items("1CST", "2CS", "2CT", "3CS", "3CT", "4CS", "4CT", "5CS", "5CT")
                        .title("Select Year")
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                btnYear.setText(text.toString());
                                btnSection.setText("Section");
                                year = text.toString();
                                getConfigSectionList();
                            }
                        })
                        .show();
            }
        });

        btnSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals(btnYear.getText().toString(), "Year")) {
                    Toast.makeText(v.getContext(), "Choose YEAR first!", Toast.LENGTH_SHORT).show();
                } else {
                    new MaterialDialog.Builder(getContext())
                            .items(sectionList)
                            .title("Select Section")
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    btnSection.setText(text.toString());
                                }
                            })
                            .show();
                }
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = edTtId.getText().toString();
                subject_name = edTtSubName.getText().toString();
                day = btnDay.getText().toString();
                period = btnPeriod.getText().toString();
                year = btnYear.getText().toString();
                section = btnSection.getText().toString();
                class_name = year + "-" + section;

                if (validate()) {
                    progressDialog.setMessage("Uploading data to server...\nInternet connection is required");
                    progressDialog.show();
                    uploadData();
                }
            }
        });

        return view;
    }

    private boolean validate() {
        // TODO: 5/7/17 validate data from editText

        return true;
    }

    private void getConfigSectionList() {
        progressDialog.setMessage("Getting data from server...\nInternet connection is required");
        progressDialog.show();
        sectionList = new ArrayList<>();
        ref = FirebaseDatabase.getInstance().getReference();
        ref = ref.child("config/class").child(year);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sectionList = (ArrayList<String>) dataSnapshot.getValue();
                progressDialog.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getConfigTime(){
        progressDialog.setMessage("Getting data from server...\nInternet connection is required");
        progressDialog.show();
        ref = FirebaseDatabase.getInstance().getReference();
        ref = ref.child("config/time").child(period);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                time = dataSnapshot.getValue(String.class);
                progressDialog.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void uploadData() {
        ref = FirebaseDatabase.getInstance().getReference();
        ref = ref.child("timetable").child(id).child(day);

        Period p = new Period(period, subject_name, time, class_name);
        ref.child(p.firstNumberPeriod()).setValue(p).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.cancel();
            }
        });

    }
}
