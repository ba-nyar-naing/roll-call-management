package com.cumdy.rcms.fragment.admin.student;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cumdy.rcms.R;

/**
 * Created by banyar on 6/4/17.
 */

public class AdminViewAllStudentsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_view_all_student, container, false);


        return view;
    }
}
