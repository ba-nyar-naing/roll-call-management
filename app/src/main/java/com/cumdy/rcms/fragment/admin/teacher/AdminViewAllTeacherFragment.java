package com.cumdy.rcms.fragment.admin.teacher;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.cumdy.rcms.R;
import com.cumdy.rcms.fragment.admin.teacher.adapter.AdminAllTeacherAdapter;
import com.cumdy.rcms.fragment.admin.teacher.adapter.AdminAllTeacherHolder;
import com.cumdy.rcms.model.Teacher;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by banyar on 6/4/17.
 */

public class AdminViewAllTeacherFragment extends Fragment {
    private RecyclerView rv;
    private DatabaseReference ref;
    private AdminAllTeacherAdapter mAdapter;

    public AdminViewAllTeacherFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_admin_view_all_teacher, container, false);

        rv = (RecyclerView) v.findViewById(R.id.rv_admin_view_all_teachers);
        ref = FirebaseDatabase.getInstance().getReference().child("teacher");
        mAdapter = new AdminAllTeacherAdapter(Teacher.class, R.layout.rc_item_teacher, AdminAllTeacherHolder.class, ref, getContext());
        RecyclerView.LayoutManager mLM = new LinearLayoutManager(getContext());
        rv.setLayoutManager(mLM);
        rv.setAdapter(mAdapter);
        return v;
    }
}
