package com.cumdy.rcms.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cumdy.rcms.R;
import com.cumdy.rcms.activity.LoginActivity;
import com.cumdy.rcms.model.Teacher;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by banyar on 5/5/17.
 */

public class ProfileFragment extends Fragment {

    private FirebaseAuth auth;
    private FirebaseUser user;
    private Button btnLogout;
    private Teacher teacher;
    private TextView txtId, txtName, txtDept, txtPost, txtEmail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        txtId = (TextView) view.findViewById(R.id.txt_profile_teacher_id);
        txtName = (TextView) view.findViewById(R.id.txt_profile_teacher_name);
        txtDept = (TextView) view.findViewById(R.id.txt_profile_teacher_department);
        txtPost = (TextView) view.findViewById(R.id.txt_profile_teacher_position);
        txtEmail = (TextView) view.findViewById(R.id.txt_profile_user_email);
        btnLogout = (Button) view.findViewById(R.id.btn_logout);

        auth = FirebaseAuth.getInstance();
        auth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    user = firebaseAuth.getCurrentUser();
                    btnLogout.setText("Log out");

                    FirebaseDatabase.getInstance().getReference().child("teacher").orderByChild("uid").equalTo(user.getUid()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                teacher = ds.getValue(Teacher.class);
                                txtId.setText(teacher.getId());
                                txtName.setText(teacher.getName());
                                txtDept.setText(teacher.getDept());
                                txtPost.setText(teacher.getPosition());
                                txtEmail.setText(teacher.getEmail());
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });

                } else {
                    txtId.setText("");
                    txtName.setText("");
                    txtDept.setText("");
                    txtPost.setText("");
                    txtEmail.setText("");
                    btnLogout.setText("Log in");
                }
            }
        });


        btnLogout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                if (auth.getCurrentUser() == null) {
                    startActivity(new Intent(v.getContext(), LoginActivity.class));
                } else {
                    auth.signOut();
                    btnLogout.setText("Log in");
                    Toast.makeText(v.getContext(), "Log out complete", Toast.LENGTH_LONG).show();

                }
            }
        });

        return view;
    }
}