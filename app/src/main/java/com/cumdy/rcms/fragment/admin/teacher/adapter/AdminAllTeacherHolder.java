package com.cumdy.rcms.fragment.admin.teacher.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by banyar on 6/4/17.
 */

public class AdminAllTeacherHolder extends RecyclerView.ViewHolder {
    public TextView txtTeacherID, txtTeacherName, txtTeacherDept, txtTeacherPost, txtTeacherMail, txtTeacherPassword;
    private String email, password;

    public AdminAllTeacherHolder(View itemView) {
        super(itemView);
        txtTeacherID = (TextView) itemView.findViewById(R.id.txt_rc_item_teacher_id);
        txtTeacherName = (TextView) itemView.findViewById(R.id.txt_rc_item_teacher_name);
        txtTeacherDept = (TextView) itemView.findViewById(R.id.txt_rc_item_teacher_dept);
        txtTeacherPost = (TextView) itemView.findViewById(R.id.txt_rc_item_teacher_position);
        txtTeacherMail = (TextView) itemView.findViewById(R.id.txt_rc_item_teacher_email);
        txtTeacherPassword = (TextView) itemView.findViewById(R.id.txt_rc_item_teacher_password);

        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(v.getContext())
                        .title("Options")
                        .items("Edit", "Delete")
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                if (TextUtils.equals(text.toString(), "Edit")) {
                                    Toast.makeText(dialog.getContext(), "Edit " + txtTeacherID.getText(), Toast.LENGTH_SHORT).show();
                                } else if (TextUtils.equals(text.toString(), "Delete")) {
                                    if (TextUtils.equals(txtTeacherName.getText().toString().toLowerCase(), "admin")) {
                                        Toast.makeText(dialog.getContext(), "Can't delete 'Admin'", Toast.LENGTH_SHORT).show();
                                    } else {
                                        email = txtTeacherMail.getText().toString();
                                        password = txtTeacherPassword.getText().toString();
                                        Toast.makeText(dialog.getContext(), "Delete " + txtTeacherID.getText(), Toast.LENGTH_SHORT).show();
                                        FirebaseDatabase.getInstance().getReference().child("teacher").child((String) txtTeacherID.getText()).removeValue();
                                        final FirebaseAuth auth = FirebaseAuth.getInstance();
                                        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<AuthResult> task) {
                                                if (task.isSuccessful()) {
                                                    auth.getCurrentUser().delete();
                                                    auth.signInWithEmailAndPassword("admin@rcms.com", "admin1");
                                                }
                                            }
                                        });

                                    }
                                }
                            }
                        })
                        .show();

                return true;
            }
        });
    }
}
