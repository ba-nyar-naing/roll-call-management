package com.cumdy.rcms.fragment.admin.student;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.cumdy.rcms.model.Student;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by banyar on 5/6/17.
 */

public class AdminAddStudentFragment extends Fragment {

    private EditText edStdRollNo, edStdName;
    private Button btnYear, btnSection, btnSubmit;
    private String rollno, name, year, section;
    private ProgressDialog progressDialog;
    private DatabaseReference ref;
    private ArrayList<String> sectionList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_add_student, container, false);

        progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setCancelable(false);

        edStdRollNo = (EditText) view.findViewById(R.id.ed_fg_admin_add_std_rollno);
        edStdName = (EditText) view.findViewById(R.id.ed_fg_admin_add_std_name);
        btnYear = (Button) view.findViewById(R.id.btn_fg_admin_add_std_year);
        btnSection = (Button) view.findViewById(R.id.btn_fg_admin_add_std_section);
        btnSubmit = (Button) view.findViewById(R.id.btn_fg_admin_add_std_submit);

        // Initialize data
        progressDialog.setMessage("Getting data from server...\nInternet connection is required");
//        getConfigData();

        btnYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items("1CST", "2CS", "2CT", "3CS", "3CT", "4CS", "4CT", "5CS", "5CT")
                        .title("Select Year")
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                btnYear.setText(text.toString());
                                btnSection.setText("Section");
                                year = btnYear.getText().toString();
                                getConfigData();
                            }
                        })
                        .show();
            }
        });

        btnSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals(btnYear.getText().toString(), "Year")) {
                    Toast.makeText(v.getContext(), "Choose YEAR first!", Toast.LENGTH_SHORT).show();
                } else {
                    new MaterialDialog.Builder(getContext())
                            .items(sectionList)
                            .title("Select Section")
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    btnSection.setText(text.toString());
                                }
                            })
                            .show();
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate(v)) {
                    progressDialog.setMessage("Uploading data to server...\nInternet connection is required");
                    progressDialog.show();
                    uploadData();
                } else {

                }
            }
        });

        return view;
    }

    private boolean validate(View v) {
        // TODO: 5/5/17 validate data from editText
        if (TextUtils.isEmpty(edStdRollNo.getText().toString().trim())) {
            Toast.makeText(v.getContext(), "Roll No must not be empty!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            rollno = edStdRollNo.getText().toString();
        }
        if (TextUtils.isEmpty(edStdName.getText().toString().trim())) {
            Toast.makeText(v.getContext(), "Name must not be empty!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            name = edStdName.getText().toString();
        }
        if (TextUtils.equals(btnYear.getText().toString(), "Year")) {
            Toast.makeText(v.getContext(), "Select year!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            year = btnYear.getText().toString();
        }
        if (TextUtils.equals(btnSection.getText().toString(), "Section")) {
            Toast.makeText(v.getContext(), "Select section!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            section = btnSection.getText().toString();
        }
        return true;
    }

    private void getConfigData() {
        progressDialog.show();
        sectionList = new ArrayList<>();
        ref = FirebaseDatabase.getInstance().getReference();
        ref = ref.child("config/class").child(year);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sectionList = (ArrayList<String>) dataSnapshot.getValue();
                progressDialog.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void uploadData() {
        ref = FirebaseDatabase.getInstance().getReference();
        ref = ref.child("students").child(year).child(section);


        Student std = new Student(rollno, name);
        ref.child(std.rollnoToNumber()).setValue(std).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.cancel();
            }
        });

    }
}
