package com.cumdy.rcms.fragment.teacher;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cumdy.rcms.R;
import com.cumdy.rcms.fragment.days.FridayFragment;
import com.cumdy.rcms.fragment.days.MondayFragment;
import com.cumdy.rcms.fragment.days.ThursdayFragment;
import com.cumdy.rcms.fragment.days.TuesdayFragment;
import com.cumdy.rcms.fragment.days.WednesdayFragment;
import com.cumdy.rcms.model.Teacher;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by banyar on 1/7/17.
 */

public class TimeTableFragment extends Fragment {

    private String teacherID, uid;
    private Teacher teacher;
    private FirebaseAuth auth;
    private Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user_timetable, container, false);

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager_user_timetable);
        final TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_days);

        bundle = new Bundle();

        auth = FirebaseAuth.getInstance();
        uid = auth.getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference().child("teacher").orderByChild("uid").equalTo(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    teacher = ds.getValue(Teacher.class);
                    teacherID = teacher.getId();

                    bundle.putString("teacherID", teacherID);
                    setupViewPager(viewPager);
                    setDefaultTab(viewPager);
                    tabLayout.setupWithViewPager(viewPager);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(), bundle);
        adapter.addFragment(new MondayFragment(), "Monday");
        adapter.addFragment(new TuesdayFragment(), "Tuesday");
        adapter.addFragment(new WednesdayFragment(), "Wednesday");
        adapter.addFragment(new ThursdayFragment(), "Thursday");
        adapter.addFragment(new FridayFragment(), "Friday");
        viewPager.setAdapter(adapter);
    }

    public void setDefaultTab(ViewPager viewPager) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case Calendar.MONDAY:
                viewPager.setCurrentItem(0);
                break;
            case Calendar.TUESDAY:
                viewPager.setCurrentItem(1);
                break;
            case Calendar.WEDNESDAY:
                viewPager.setCurrentItem(2);
                break;
            case Calendar.THURSDAY:
                viewPager.setCurrentItem(3);
                break;
            case Calendar.FRIDAY:
                viewPager.setCurrentItem(4);
                break;
            default:
                viewPager.setCurrentItem(0);
                break;
        }

    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private final Bundle fragmentBundle;


        public ViewPagerAdapter(FragmentManager fm, Bundle bundle) {
            super(fm);
            fragmentBundle = bundle;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f = mFragmentList.get(position);
            f.setArguments(this.fragmentBundle);
            return f;
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }
}
