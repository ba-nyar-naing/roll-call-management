package com.cumdy.rcms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cumdy.rcms.R;

/**
 * Created by banyar on 6/4/17.
 */

public class TimetableHolder extends RecyclerView.ViewHolder {
    public TextView txtPeriodNo, txtPeriodSubjectName, txtPeriodTime, txtPeriodClassName;

    public TimetableHolder(View itemView) {
        super(itemView);
        txtPeriodNo = (TextView) itemView.findViewById(R.id.txt_rc_item_period_no);
        txtPeriodSubjectName = (TextView) itemView.findViewById(R.id.txt_rc_item_period_subject_name);
        txtPeriodTime = (TextView) itemView.findViewById(R.id.txt_rc_item_period_time);
        txtPeriodClassName = (TextView) itemView.findViewById(R.id.txt_rc_item_period_class_name);
    }
}
