package com.cumdy.rcms.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cumdy.rcms.R;
import com.cumdy.rcms.model.Period;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;


/**
 * Created by banyar on 5/5/17.
 */

public class TimetableAdapter extends FirebaseRecyclerAdapter<Period, TimetableHolder> {

    private Context context;

    public TimetableAdapter(Class<Period> modelClass, int modelLayout, Class<TimetableHolder> viewHolder, DatabaseReference ref, Context context) {
        super(modelClass, modelLayout, viewHolder, ref);
        this.context = context;
    }

    @Override
    protected void populateViewHolder(TimetableHolder holder, Period p, int position) {
        holder.txtPeriodNo.setText(p.getPeriod_no());
        holder.txtPeriodSubjectName.setText(p.getSubject_name());
        holder.txtPeriodTime.setText(p.getTime());
        holder.txtPeriodClassName.setText(p.getClass_name());

    }

}