package com.cumdy.rcms.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.cumdy.rcms.activity.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by banyar on 4/29/17.
 */

public class RollCallerRCAdapter extends RecyclerView.Adapter<RollCallerRCAdapter.ViewHolder> {

    private List<String> list = new ArrayList<>();
    private ArrayList<String> presentList = new ArrayList<>();
    private ArrayList<String> skipList = new ArrayList<>();
    private List<String> recallSkipList = new ArrayList<>();
    private ArrayList<String> absentList = new ArrayList<>();
    private List<String> undoStdList = new ArrayList<>();
    private ArrayList<String> undoActionList = new ArrayList<>();
    private Context context;
    private TextView txtPresent, txtSkip, txtAbsent, txtTotalStd;
    private Button btnCallSkippedStd, btnUndo, btnComplete;
    private String path;
    private DatabaseReference ref;
    private ProgressDialog progressDialog;

    public RollCallerRCAdapter(List<String> list, Context context, TextView txtTotalStd, TextView txtPresent, TextView txtSkip, TextView txtAbsent, Button btnCallSkippedStd, Button btnUndo, Button btnComplete, String path) {
        this.list = list;
        this.context = context;
        this.txtTotalStd = txtTotalStd;
        this.txtPresent = txtPresent;
        this.txtSkip = txtSkip;
        this.txtAbsent = txtAbsent;
        this.btnCallSkippedStd = btnCallSkippedStd;
        this.btnUndo = btnUndo;
        this.btnComplete = btnComplete;
        this.path = path;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rc_item_student, parent, false);
        ViewHolder holder = new ViewHolder(v);

        btnCallSkippedStd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recallSkippedStudents(recallSkipList);
                txtSkip.setText("0");
                recallSkipList = new ArrayList<>();
                skipList = new ArrayList<>();
                Toast.makeText(context, "Add skipped students to list", Toast.LENGTH_SHORT).show();
            }
        });

        btnUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUndo();
            }
        });

        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                progressDialog = new ProgressDialog(v.getContext());
                progressDialog.setMessage("Uploading data to server... \nInternet connection is required");
                progressDialog.setCancelable(false);

                if (onComplete(txtTotalStd, txtPresent, txtSkip, txtAbsent)) {
                    progressDialog.show();

                    // Connection to server here.
                    ref = FirebaseDatabase.getInstance().getReference();
                    ref = ref.child("rollcall").child(path);
                    String[] rollNoAndName;
                    String rollNo;

                    for (String text : absentList) {
                        rollNoAndName = text.split(":");
                        rollNo = rollNoAndName[0];
                        ref.child(rollNo).setValue("A");
                    }

                    String text;
                    for (int i = 0; i < presentList.size(); i++) {
                        text = presentList.get(i);
                        rollNoAndName = text.split(":");
                        rollNo = rollNoAndName[0];
                        if (i == presentList.size() - 1) {
                            ref.child(rollNo).setValue("P").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressDialog.cancel();
                                    if (task.isSuccessful()) {
                                        Toast.makeText(context, "Upload complete", Toast.LENGTH_SHORT).show();
                                        final Context ctx = v.getContext();
                                        new MaterialDialog.Builder(ctx)
                                                .title("Complete")
                                                .content("Upload complete\nPress OK to redirect home")
                                                .positiveText("Ok")
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        ctx.startActivity(new Intent(context, MainActivity.class));
                                                    }
                                                })
                                                .show();
                                    } else {
                                        Toast.makeText(context, "Upload fail. Try again", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            ref.child(rollNo).setValue("P");
                        }
                    }
                }
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final boolean[] singleClick = {true};
        final String data = list.get(position);
        String[] rollnoAndName = data.split(":");
        String rollno = rollnoAndName[0];
        String name = rollnoAndName[1];
        holder.rollno.setText(rollno);
        holder.name.setText(name);
        holder.btnPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (singleClick[0]) {
                    singleClick[0] = false;
                    remove(data);
                    presentList.add(data);
                    undoActionList.add("Present");
                    undoStdList.add(data);
                    int c = Integer.valueOf(txtPresent.getText().toString()) + 1;
                    txtPresent.setText(String.valueOf(c));
                    txtPresent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new MaterialDialog.Builder(v.getContext())
                                    .title("Present students")
                                    .items(presentList)
                                    .positiveText("OK")
                                    .show();
                        }
                    });
                }
            }
        });
        holder.btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (singleClick[0]) {
                    singleClick[0] = false;
                    remove(data);
                    skipList.add(data);
                    recallSkipList.add(data);
                    undoActionList.add("Skip");
                    undoStdList.add(data);
                    int c = Integer.valueOf(txtSkip.getText().toString()) + 1;
                    txtSkip.setText(String.valueOf(c));
                    txtSkip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Integer.valueOf(txtSkip.getText().toString()) != 0)
                                new MaterialDialog.Builder(v.getContext())
                                        .title("Skip students")
                                        .items(skipList)
                                        .positiveText("OK")
                                        .show();
                        }
                    });
                }
            }
        });
        holder.btnAbsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (singleClick[0]) {
                    singleClick[0] = false;
                    remove(data);
                    absentList.add(data);
                    undoActionList.add("Absent");
                    undoStdList.add(data);
                    int c = Integer.valueOf(txtAbsent.getText().toString()) + 1;
                    txtAbsent.setText(String.valueOf(c));
                    txtAbsent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new MaterialDialog.Builder(v.getContext())
                                    .title("Absent students")
                                    .items(absentList)
                                    .positiveText("OK")
                                    .show();
                        }
                    });
                }
            }
        });
    }

    public void onUndo() {
        if (!undoActionList.isEmpty() && !undoStdList.isEmpty()) {
            int lastIndex = undoActionList.size() - 1;
            String action = undoActionList.get(lastIndex);
            undoActionList.remove(lastIndex);
            String std = undoStdList.get(lastIndex);
            undoStdList.remove(lastIndex);
            if (action.equals("Present")) {
                insert(std);
                presentList.remove(presentList.size() - 1);
                int c = Integer.valueOf(txtPresent.getText().toString()) - 1;
                txtPresent.setText(String.valueOf(c));
            } else if (action.equals("Skip")) {
                if (!recallSkipList.isEmpty()) {
                    recallSkipList.remove(recallSkipList.size() - 1);
                }
                if (!skipList.isEmpty()) {
                    skipList.remove(skipList.size() - 1);
                    insert(std);
                    int c = Integer.valueOf(txtSkip.getText().toString()) - 1;
                    txtSkip.setText(String.valueOf(c));
                }
            } else {
                insert(std);
                absentList.remove(absentList.size() - 1);
                int c = Integer.valueOf(txtAbsent.getText().toString()) - 1;
                txtAbsent.setText(String.valueOf(c));
            }
        }
    }

    public boolean onComplete(TextView txtTotal, TextView txtPresent, TextView txtSkip, TextView txtAbsent) {
        int total = Integer.valueOf(txtTotal.getText().toString());
        int present = Integer.valueOf(txtPresent.getText().toString());
        int skip = Integer.valueOf(txtSkip.getText().toString());
        int absent = Integer.valueOf(txtAbsent.getText().toString());
        if (total == (present + absent) && skip == 0)
            return true;
        return false;
    }

    public void recallSkippedStudents(List<String> skipList) {
        for (String std : skipList) {
            insert(std);
        }
        for (int i = 0; i < undoActionList.size(); i++) {
            if (undoActionList.get(i).equals("Skip")) {
                undoActionList.remove(i);
                undoStdList.remove(i);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(String data) {
        list.add(data);
        notifyItemInserted(getItemCount());
    }

    public void remove(String data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView rollno, name;
        private Button btnPresent, btnSkip, btnAbsent;

        public ViewHolder(View itemView) {
            super(itemView);
            rollno = (TextView) itemView.findViewById(R.id.txt_rc_item_student_roll_no);
            name = (TextView) itemView.findViewById(R.id.txt_rc_item_student_name);
            btnPresent = (Button) itemView.findViewById(R.id.btn_rc_item_present);
            btnSkip = (Button) itemView.findViewById(R.id.btn_rc_item_skip);
            btnAbsent = (Button) itemView.findViewById(R.id.btn_rc_item_absent);
        }

    }

}