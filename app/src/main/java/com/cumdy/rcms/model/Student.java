package com.cumdy.rcms.model;

/**
 * Created by banyar on 4/29/17.
 */

public class Student {
    private String rollno, name;

    public Student() {
    }

    public Student(String rollno, String name) {
        this.rollno = rollno;
        this.name = name;
    }

    public String toString() {
        return rollno + ":" + name;
    }

    public String rollnoToNumber() {
        String[] rollnoAndName = rollno.split("-");
        return rollnoAndName[1];

    }

    public String getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
