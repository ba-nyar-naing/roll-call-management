package com.cumdy.rcms.model;

/**
 * Created by banyar on 4/29/17.
 */

public class Teacher {
    private String id;
    private String name;
    private String dept;
    private String position;
    private String email;
    private String password;
    private String uid;


    public Teacher() {

    }

    public Teacher(String id, String name, String dept, String position, String email, String password, String uid) {
        this.id = id;
        this.name = name;
        this.dept = dept;
        this.position = position;
        this.email = email;
        this.password = password;
        this.uid = uid;
    }

    public Teacher(String id, String name, String dept, String position, String email, String password) {
        this.id = id;
        this.name = name;
        this.dept = dept;
        this.position = position;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
