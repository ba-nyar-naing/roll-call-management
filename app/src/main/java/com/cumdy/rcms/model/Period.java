package com.cumdy.rcms.model;

/**
 * Created by banyar on 5/2/17.
 */

public class Period {
    private String period_no, subject_name, time, class_name;

    public Period() {

    }

    public Period(String no, String subject_name, String time, String class_name) {
        this.period_no = no;
        this.subject_name = subject_name;
        this.time = time;
        this.class_name = class_name;
    }

    public String firstNumberPeriod() {
        return String.valueOf(period_no.charAt(0));
    }

    public String getPeriod_no() {
        return period_no;
    }

    public void setPeriod_no(String period_no) {
        this.period_no = period_no;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }
}
