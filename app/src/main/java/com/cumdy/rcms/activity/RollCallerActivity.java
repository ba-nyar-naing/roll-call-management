package com.cumdy.rcms.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.cumdy.rcms.adapter.RollCallerRCAdapter;
import com.cumdy.rcms.model.Student;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RollCallerActivity extends AppCompatActivity {

    private String day, month, period, year, section, path;
    private RecyclerView recyclerView;
    private RollCallerRCAdapter adapter;
    private TextView txtTotalStd, txtPresent, txtSkip, txtAbsent;
    private Button btnCallSippedStd, btnUndo, btnComplete;
    private ArrayList<String> studentNameList;
    private List<Student> studentList;
    private DatabaseReference ref;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roll_caller);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Initializing data from server... \nInternet connection is required");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);
        progressDialog.show();


        Bundle bundle = getIntent().getExtras();
        day = bundle.getString("day");
        month = bundle.getString("month");
        period = bundle.getString("period");
        year = bundle.getString("year");
        section = bundle.getString("section");
        path = month + "/" + day + "/" + year + "/" + section + "/" + period;
        toolbar.setTitle("Class " + year + " -  " + section + " : Period - " + period);
        setSupportActionBar(toolbar);

        txtTotalStd = (TextView) findViewById(R.id.txt_roll_caller_total_std);
        txtPresent = (TextView) findViewById(R.id.txt_roll_caller_present_std);
        txtSkip = (TextView) findViewById(R.id.txt_roll_caller_skip_std);
        txtAbsent = (TextView) findViewById(R.id.txt_roll_caller_absent_std);
        btnCallSippedStd = (Button) findViewById(R.id.btn_roll_caller_call_skipped_students);
        btnUndo = (Button) findViewById(R.id.btn_roll_caller_undo);
        btnComplete = (Button) findViewById(R.id.btn_roll_caller_complete);

        ref = FirebaseDatabase.getInstance().getReference();
        ref.child("students").child(year).child(section).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("studentNameList", dataSnapshot.getValue().toString());
//                studentNameList = (ArrayList<String>) dataSnapshot.getValue();
//                studentList = (List<Student>) dataSnapshot.getValue();
//                testList = (HashMap<String, String>) dataSnapshot.getValue();
                studentList = new ArrayList<>();
                studentNameList = new ArrayList<String>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    studentList.add(ds.getValue(Student.class));
                }

                studentNameList = studentToString(studentList);
                if (studentNameList != null) {
                    txtTotalStd.setText(String.valueOf(studentNameList.size()));

                    recyclerView = (RecyclerView) findViewById(R.id.recycler_view_roll_caller);
                    adapter = new RollCallerRCAdapter(studentNameList, getApplication(), txtTotalStd, txtPresent, txtSkip, txtAbsent, btnCallSippedStd, btnUndo, btnComplete, path);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                    RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
                    itemAnimator.setAddDuration(500);
                    itemAnimator.setRemoveDuration(500);
                    recyclerView.setItemAnimator(itemAnimator);


                    txtTotalStd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new MaterialDialog.Builder(v.getContext())
                                    .title("Total students")
                                    .items(studentNameList)
                                    .positiveText("OK")
                                    .show();
                        }
                    });
                }
                progressDialog.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    private ArrayList<String> studentToString(List<Student> data) {
        ArrayList<String> stdList = new ArrayList<>();
        for (Student std : data) {
            stdList.add(std.toString());
        }
        return stdList;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        new MaterialDialog.Builder(this)
                .title("Cancel")
                .content("Are you sure to cancel?")
                .positiveText("Stop Calling")
                .positiveColor(getResources().getColor(R.color.red))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        RollCallerActivity.super.onBackPressed();
                    }
                })
                .negativeText("Close")
                .show();


    }
}
