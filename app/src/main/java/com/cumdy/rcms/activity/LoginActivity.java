package com.cumdy.rcms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by banyar on 1/7/17.
 */

public class LoginActivity extends AppCompatActivity {

    private EditText edUsername, edPassword;
    private String email, password;
    private Button btnLogin;
    private FirebaseAuth auth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.activity_login);

        edUsername = (EditText) findViewById(R.id.input_username_or_email);
        edPassword = (EditText) findViewById(R.id.input_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Authenticating...");
        progressDialog.setCancelable(false);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = edUsername.getText().toString();
                password = edPassword.getText().toString();
                progressDialog.show();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                    return;
                }

                if (password.length() < 6) {
                    edPassword.setError("Password must be longer than 6");
                    progressDialog.cancel();
                    return;
                }

                //authenticate user
                auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                 if (task.isSuccessful()) {
                                    progressDialog.cancel();
                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    i.putExtra("email", email);
                                    i.putExtra("password", password);
                                    startActivity(i);
                                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                    finish();
                                } else {
                                    progressDialog.cancel();
                                    Toast.makeText(LoginActivity.this, "Authentication failed", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title("Exit")
                .content("Are you sure to exit?")
                .positiveText("Exit")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finishAffinity();
                    }
                })
                .negativeText("Cancel")
                .show();
    }
}

//
//    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
//
//        private String resp;
//        ProgressDialog progressDialog;
//
//        @Override
//        protected String doInBackground(String... params) {
//            publishProgress("Sleeping...", "Slept"); // Calls onProgressUpdate()
//
//            try {
//                Thread.sleep(1000);
//                resp = params[0] + " " + params[1];
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//                resp = e.getMessage();
//            } catch (Exception e) {
//                e.printStackTrace();
//                resp = e.getMessage();
//            }
////            Log.i("Response doInBg", resp);
//            return resp;
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            // execution of result of Long time consuming operation
////            Log.i("Response onPostExe", result);
//            progressDialog.dismiss();
//            Intent i = new Intent(LoginActivity.this, MainActivity.class);
//            i.putExtra("nav", R.id.nav_account_student);
//            startActivity(i);
////            finalResult.setText(result);
//        }
//
//
//        @Override
//        protected void onPreExecute() {
////            Log.i("Response onPreExe", "cuting");
//            progressDialog = new ProgressDialog(LoginActivity.this);
//            progressDialog.setMessage("Authenticating...");
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.show();
//        }
//
//
//        @Override
//        protected void onProgressUpdate(String... text) {
//
////            Log.i("Response onProgUd", text[0] + " " + text[1]);
////            finalResult.setText(text[0]);
//
//        }
//    }


