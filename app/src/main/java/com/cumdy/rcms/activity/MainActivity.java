package com.cumdy.rcms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cumdy.rcms.R;
import com.cumdy.rcms.fragment.AboutFragment;
import com.cumdy.rcms.fragment.ProfileFragment;
import com.cumdy.rcms.fragment.SettingFragment;
import com.cumdy.rcms.fragment.admin.AdminAddAnnouncementFragment;
import com.cumdy.rcms.fragment.admin.AdminAddTimeTableFragment;
import com.cumdy.rcms.fragment.admin.AdminHomeFragment;
import com.cumdy.rcms.fragment.admin.student.AdminStudentFragment;
import com.cumdy.rcms.fragment.admin.teacher.AdminTeacherFragment;
import com.cumdy.rcms.fragment.teacher.AnnouncementFragment;
import com.cumdy.rcms.fragment.teacher.HomeFragment;
import com.cumdy.rcms.fragment.teacher.RollCallFragment;
import com.cumdy.rcms.fragment.teacher.TimeTableFragment;
import com.cumdy.rcms.model.Teacher;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private static boolean isAdmin;
    private DrawerLayout drawerLayout;
    private TextView txtNavHeaderName;
    private int defaultID = R.id.nav_home;
    private Class defaultFragment = TimeTableFragment.class;
    private FirebaseUser user;
    private String name = "User";
    private Menu menu;
    private Teacher teacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getIntent().hasExtra("nav"))
            defaultID = getIntent().getExtras().getInt("nav");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View navHeaderView = navigationView.getHeaderView(0);
        txtNavHeaderName = (TextView) navHeaderView.findViewById(R.id.txtWelcomeUser);
        menu = navigationView.getMenu();
        setupDrawerContent(navigationView);
        selectDrawerItem(defaultID);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null)
            startActivity(new Intent(MainActivity.this, LoginActivity.class));

        auth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    updateUI(user);
                }
            }
        });

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                selectDrawerItem(menuItem.getItemId());
                drawerLayout.closeDrawers();
                menuItem.setChecked(true);
                if (menuItem.getItemId() != R.id.nav_exit)
                    setTitle(menuItem.getTitle());
                return true;
            }
        });
    }

    private void updateUI(FirebaseUser user) {
        if (isAdmin) {
            if (TextUtils.equals(user.getEmail(), "admin@rcms.com")) {
                isAdmin = true;
                menu.setGroupVisible(R.id.upper_admin, true);
                menu.setGroupVisible(R.id.upper_user, false);
                txtNavHeaderName.setText(R.string.welcome_admin);
                return;
            } else {
                isAdmin = false;
            }

        }
        FirebaseDatabase.getInstance().getReference().child("teacher").orderByChild("uid").equalTo(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    teacher = ds.getValue(Teacher.class);
                    name = teacher.getName();

                    if (name.equals("Admin")) {
                        isAdmin = true;
                        menu.setGroupVisible(R.id.upper_admin, true);
                        menu.setGroupVisible(R.id.upper_user, false);
                        txtNavHeaderName.setText(R.string.welcome_admin);
                    } else {
                        menu.setGroupVisible(R.id.upper_user, true);
                        menu.setGroupVisible(R.id.upper_admin, false);
                        txtNavHeaderName.setText("Welcome " + name);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void selectDrawerItem(int id) {
        Fragment fragment = null;
        switch (id) {
            case R.id.nav_home:
                defaultID = R.id.nav_home;
                defaultFragment = HomeFragment.class;
                break;
            case R.id.nav_timetable:
                defaultID = R.id.nav_timetable;
                defaultFragment = TimeTableFragment.class;
                break;
            case R.id.nav_roll_call:
                defaultID = R.id.nav_roll_call;
                defaultFragment = RollCallFragment.class;
                break;
            case R.id.nav_announcement:
                defaultID = R.id.nav_announcement;
                defaultFragment = AnnouncementFragment.class;
                break;
            case R.id.nav_admin_home:
                defaultID = R.id.nav_admin_home;
                defaultFragment = AdminHomeFragment.class;
                break;
            case R.id.nav_admin_add_timetable:
                defaultID = R.id.nav_admin_add_timetable;
                defaultFragment = AdminAddTimeTableFragment.class;
                break;
            case R.id.nav_admin_teacher:
                defaultID = R.id.nav_admin_teacher;
                defaultFragment = AdminTeacherFragment.class;
                break;
            case R.id.nav_admin_student:
                defaultID = R.id.nav_admin_student;
                defaultFragment = AdminStudentFragment.class;
                break;
            case R.id.nav_admin_add_announcement:
                defaultID = R.id.nav_admin_add_announcement;
                defaultFragment = AdminAddAnnouncementFragment.class;
                break;
            case R.id.nav_profile:
                defaultID = R.id.nav_profile;
                defaultFragment = ProfileFragment.class;
                break;
            case R.id.nav_setting:
                defaultID = R.id.nav_setting;
                defaultFragment = SettingFragment.class;
                break;
            case R.id.nav_about:
                defaultID = R.id.nav_about;
                defaultFragment = AboutFragment.class;
                break;
            case R.id.nav_exit:
                checkExit();
                break;
            default:
                defaultID = R.id.nav_home;
                defaultFragment = HomeFragment.class;
                break;
        }
        try {
            fragment = (Fragment) defaultFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.containerView, fragment).commit();
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            checkExit();
        }
    }

    private void checkExit() {
        new MaterialDialog.Builder(this)
                .title("Exit")
                .content("Are you sure to exit?")
                .positiveText("Exit")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finishAffinity();
                    }
                })
                .negativeText("Cancel")
                .show();
    }

}
